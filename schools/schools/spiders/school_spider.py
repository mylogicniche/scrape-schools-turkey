import scrapy
from string import punctuation

class SchoolSpider(scrapy.Spider):
    name = "schools"

    start_urls = [
        'https://www.meb.gov.tr/baglantilar/okullar/index.php?ILKODU=34',
    ]

    key_list = [
        "Kurum Kodu", "Kurum Adı",
        "Telefon", "Fax", "Eposta", "WEB", "Adres", "Vizyon", "Misyon",
        "Başarılar", "Öğretmen", "Rehber Öğretmen", "Öğrenci", "Derslik",
        "Müzik Sınıfı", "Resim Sınıfı", "BT Sınıfı", "Misafirhane", "Kütüphane",
        "Fen Labaratuarı", "Hazırlık Sınıfı", "Spor Salonu", "Yemekhane", "Öğrenci",
        "Derslik", "Kütüphane", "Konferans Salonu", "Öğretim Şekli", "Revir",
        "Bahçe", "Saatler", "Bağlantı", "Yabancı Dil", "Isınma", "Lojman", "Servis",
        "Ulaşım", "Yerleşim Yeri", "Kontenjan", "Sportif Etkinlikler", "Bilimsel Etkinlikler",
        "Proje Çalışmaları",
    ]

    def parse(self, response):
        links = response.xpath("//table[contains(@class, 'table')]//a[contains(@href,'hakkinda')]/@href").extract()
        for link in links:
            yield scrapy.Request(link, callback=self.parse_about)
        next_page = response.xpath("//a[contains(@class, 'next')]/@href").extract_first()
        #print(links)
        yield response.follow(next_page, callback=self.parse)

    def parse_about(self, response):
        res = {}
        tr_en_char = {
                'ç': 'c',
                'ğ': 'g',
                'ö': 'o',
                'ş': 's',
                'ü': 'u',
                'ı': 'i',
                'Ç': 'C',
                'Ğ': 'G',
                'Ö': 'O',
                'Ş': 'S',
                'Ü': 'U',
                'İ': 'I'
                }
        translator = str.maketrans('', '', punctuation)
        for key in self.key_list:
            path = "//*[contains(.,'{0}')]/following-sibling::*[1]/text()".format(key)
            val = response.xpath(path).extract_first()
            if val:
                if not val.translate(translator):
                    path = "//*[contains(.,'{0}')]/following-sibling::*[2]/text()".format(key)
                    val = response.xpath(path).extract_first()
                for k, v in tr_en_char.items():
                    if k in val:
                        val = val.replace(k, v)
                val = val.replace('\n', ' ').replace('\r', '').replace('\t', '')

            for k, v in tr_en_char.items():
                if k in key:
                    key = key.replace(k, v)
            res.update({key: val})

        # key = response.xpath("//td[normalize-space()=':']//preceding-sibling::td[1]/text()").extract()
        # print("len_key: ", len(key))
        # val_el = response.xpath("//td[normalize-space()=':']//following-sibling::td[1]//text()")
        # val = [el if el is not None else '' for el in val_el.extract()]
        # print("len_val: ", len(val))

        # res = {}
        # if (len(key) == len(val)) and (len(key) and (len(val))):
        #     for k, v in key, val:
        #         res.update({k.strip(): v.strip()})
        url = response.request.url
        yield {url:res}
